# test_qa_bots.py
import pytest  # noqa
import pandas as pd
import time

from qary.skills.qa_bots import Bot
from qary.qa.qa_datasets import get_bot_accuracies

import logging

log = logging.getLogger(__name__)


__author__ = "SEE AUTHORS.md"
__copyright__ = "Hobson Lane"
__license__ = "The Hippocratic License, see LICENSE.txt (MIT + Do no Harm)"


def test_qa_bots():
    bot = Bot()
    assert callable(bot.reply)
    
    t0 = time.time()
    replies = bot.reply('Hi', context='')
    t1 = time.time()
    assert len(replies) == 0
    assert t1 - t0 < 5
    
    t0 = time.time()
    obama_replies = bot.reply(
        'When was Obama born?',
        context={'doc': {'text': 'Barack Obama was born in 1961.'}})
    t1 = time.time()
    assert len(obama_replies) > 0
    assert t1 - t0 < 5
    
    t0 = time.time()
    obama_replies = sorted(obama_replies)
    t1 = time.time()
    assert obama_replies[-1][0] > .9
    assert float(obama_replies[-1][1]) == 1961
    assert t1 - t0 < 5

    t0 = time.time()
    obama_replies = bot.reply(
        'Where was Barack born?',
        context='Barack Obama was born in Hawaii. O. was born in Uzbekistan. M. was born in Ukraine.')
    t1 = time.time()
    assert len(obama_replies) > 0
    assert t1 - t0 < 5

    t0 = time.time()
    obama_replies = sorted(obama_replies)
    t1 = time.time()
    assert obama_replies[-1][0] > .3
    assert obama_replies[-1][1].lower().startswith('hawaii')
    assert t1 - t0 < 5

def test_qa_bot_accuracy():
    bot = Bot()
    df = pd.DataFrame(get_bot_accuracies(bot))
    q_accuracies = df.groupby('question')['bot_accuracy'].max()
    mean_acc = q_accuracies.mean()
    log.warning(f'Mean bot accuracy: {mean_acc}')
    assert mean_acc > .4
